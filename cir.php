<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/21/2018
 * Time: 1:51 PM
 */

include_once "vendor/autoload.php";

use Pondit\Calculator\AreaCalculator\Circle;
use Pondit\Calculator\AreaCalculator\Displayer;

$circle1 = new Circle();
$circle1->pi = 3.14159;
$circle1->radius = 6;


$displayer=new Displayer();
$displayer->displaypre($circle1->getCir());
$displayer->displayH1($circle1->getCir());